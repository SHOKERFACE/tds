// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS/Weapon/Projectiles/ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(NetMulticast, Reliable)
	void SpawnExploseFX_Multicast(UParticleSystem* FXTemplate);
	
	UFUNCTION(NetMulticast, Reliable)
	void SpawnExploseSound_Multicast(USoundBase* ExplodeSound);
	
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void TimerExplose(float DeltaTime);

	virtual void BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	
	virtual void ImpactProjectile() override;

	
	void Explose();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Grenade")
	bool TimerEnabled = false;
	
	float TimerToExplose = 0.0f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Grenade")
	float TimeToExplose = 2.5f;
};
