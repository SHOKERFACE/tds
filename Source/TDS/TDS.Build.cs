// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS : ModuleRules
{

	public TDS(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "PhysicsCore" ,"Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule","Slate" });
        
        PublicIncludePaths.AddRange(new string[]{"TDS/"});
    }
	
	
}
